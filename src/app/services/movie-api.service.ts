import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class MovieApiService {

  constructor(private http:HttpClient) { }

  baseUrl = "https://api.themoviedb.org/3";
  apiKey = "f77172a2eee920087e11543c4cd02295";

  bannerApiData():Observable<any>{
    return this.http.get(`${this.baseUrl}/trending/all/week?api_key=${this.apiKey}`);
  }

  trendingMovieApiData():Observable<any>{
    return this.http.get(`${this.baseUrl}/trending/movie/day?api_key=${this.apiKey}`);
  }

  getSearchMovie(data:any):Observable<any>{
    return this.http.get(`${this.baseUrl}/search/movie?api_key=${this.apiKey}&query=${data.movieName}`);
  }

  getMovieDetails(data:any):Observable<any>{
    return this.http.get(`${this.baseUrl}/movie/${data}?api_key=${this.apiKey}`);
  }

  getMovieVideo(data:any):Observable<any>{
    return this.http.get(`${this.baseUrl}/movie/${data}/videos?api_key=${this.apiKey}`);
  }

  getMovieCast(data:any):Observable<any>{
    return this.http.get(`${this.baseUrl}/movie/${data}/credits?api_key=${this.apiKey}`);
  }

  getActionMovie():Observable<any>{
    return this.http.get(`${this.baseUrl}/discover/movie?api_key=${this.apiKey}&with_genres=20`);
  }

  getComedyMovie():Observable<any>{
    return this.http.get(`${this.baseUrl}/discover/movie?api_key=${this.apiKey}&with_genres=35`);
  }

  getAnimationMovie():Observable<any>{
    return this.http.get(`${this.baseUrl}/discover/movie?api_key=${this.apiKey}&with_genres=16`);
  }

  getDocumantoryMovie():Observable<any>{
    return this.http.get(`${this.baseUrl}/discover/movie?api_key=${this.apiKey}&with_genres=99`)
  }


}
