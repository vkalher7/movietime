import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MovieApiService } from 'src/app/services/movie-api.service';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.css']
})
export class MovieDetailsComponent implements OnInit{

  getMovieDetailResults:any;
  getMovieVideoResult:any;
  getMovieCastResult:any;
  movieOverview:string='';
  
  constructor(private service:MovieApiService,private router:ActivatedRoute){}

  ngOnInit(): void {
    let getParamId = this.router.snapshot.paramMap.get('id');
    console.log(getParamId,'paramId#');
    this.getMovie(getParamId);
    this.getVideo(getParamId);
    this.getCast(getParamId);
  }

  getMovie(id:any){
    if(localStorage.getItem(id) != null){  
      this.getMovieDetailResults = JSON.parse(localStorage.getItem(id) || '{}');
    }else{
      this.service.getMovieDetails(id).subscribe((result)=>{
        console.log(result,'movieDetails#');
        this.getMovieDetailResults = result;
        localStorage.setItem(JSON.stringify(this.getMovieDetailResults.id),JSON.stringify(this.getMovieDetailResults));
      });
    }
  }

  getVideo(id:any){
    this.service.getMovieVideo(id).subscribe((result)=>{
      console.log(result,'movieVideo#');
      result.results.forEach((element:any) => {
        if(element.type=="Trailer"){
          this.getMovieVideoResult = element.key;
          //console.log('trailor key path',this.getMovieVideoResult);
        }
      });
    });
  }

  getCast(id:any){
    this.service.getMovieCast(id).subscribe((result)=>{
      console.log(result,'movieCast#');
        this.getMovieCastResult = result.cast;      
    });
  }


}
