import { Component, OnInit } from '@angular/core';
import { MovieApiService } from 'src/app/services/movie-api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit{

  bannerResult:any =[];
  trendingMovieResult:any = [];
  documantoryMovieResult:any = [];
  comedyMovieResult:any = [];
  animationMovieResult:any = [];

  movieKey:any;
  constructor(private service:MovieApiService){}

  ngOnInit(): void {
    this.bannerData();
    this.trendingData();
    this.documantoryMovieData();
    this.comedyMovieData();
    this.animationMovieData();
  }

  bannerData(){
    this.service.bannerApiData().subscribe((result) => {
      console.log(result,'bannerresult#');
      this.bannerResult = result.results;
    });
  }

  trendingData(){
    this.service.trendingMovieApiData().subscribe((result)=> {
      this.trendingMovieResult = result.results;
      console.log(result,'trendingresults#');
    })
  }

  documantoryMovieData(){
    this.service.getDocumantoryMovie().subscribe((result)=> {
      this.documantoryMovieResult = result.results;
      console.log(result,'documantoryMovieResults#');
    });
  }

  comedyMovieData(){
    this.service.getComedyMovie().subscribe((result)=> {
      this.comedyMovieResult = result.results;
      console.log(result,'comedyMovieResults#');
    });
  }

  animationMovieData(){
    this.service.getAnimationMovie().subscribe((result)=> {
      this.animationMovieResult = result.results;
      console.log(result,'animationMovieResults#');
    });
  }

}
